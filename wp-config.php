<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test01');

/** MySQL database username */
define('DB_USER', 'test01');

/** MySQL database password */
define('DB_PASSWORD', 'test01');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fwgY@,*d#9~U.$(0|%+{_bal?cx)dy[-P(,)U J,6z44**=e5%OqGHPUsL]%LG0k');
define('SECURE_AUTH_KEY',  'ZW~uGX~^S4*mp4--u7+$ugcH^Wv~TcN0soX/&v3jf[SDlK&[t(L!X@`y`x8h6F&:');
define('LOGGED_IN_KEY',    'd($6{ihM [~5mao5s hOE6/x}eaxThnA5rc~27)i3z-OmR%sTKD[1.zmE)bVIK6*');
define('NONCE_KEY',        'L@bac{:Ts!p|S#6T0p_,8Zt6i6`!Z`X6l~Nf.s.HHzK!RfFNk2(.cO4*&(N:P+0,');
define('AUTH_SALT',        '8?Q0C^ZaX3ZX=f!O^E)s$&<`A^c3tC@F^o/8~THsM7&SJbu3dS>>te:8MbTj@Fn2');
define('SECURE_AUTH_SALT', ':-#4&{|R8pJ/<{Q=}f $^Wh}NaL9LO7ceh1pl#hs,xKFeCb&&vFqOtY7td>)Q-v ');
define('LOGGED_IN_SALT',   ';E*J&/>QgT/;Awll-mU|FZN.q`1[3s{sRhV{[.hYx;j8pHjYK>}SU%v A)uW_YtB');
define('NONCE_SALT',       'MnvKu=q=@$_&:E2>=`]YH(oXEQ)<2Z#@Vyr#j.|e,h:b.7x[4BLaqaQs&t5PFy54');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
